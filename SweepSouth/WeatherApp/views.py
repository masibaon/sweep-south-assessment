from django.shortcuts import render
from django.http import HttpResponse
from .models import Location
from django.db import connection
import requests
import json
import time
import math
from decimal import Decimal

# Create your views here.
def index(request):
    return HttpResponse("Hello, world.")

def weatherApiCall(request):
	#extract location details
	currLocationArr = request.GET['current_location'].split(',')
	bookLocationArr = request.GET['booking_location'].split(',')
	eTime = (math.trunc(time.time()))
	
	#compile location Weather API string
	strCurrWeatherApi = 'https://api.darksky.net/forecast/ae4445788bd9de0cbfe305d9fa0eab51/'+currLocationArr[0]+','+currLocationArr[1]+','+str(eTime)
	strBookWeatherApi = 'https://api.darksky.net/forecast/ae4445788bd9de0cbfe305d9fa0eab51/'+bookLocationArr[0]+','+bookLocationArr[1]+','+str(eTime)
	
	weatherApiArr = []
	weatherApiArr.append({'weatherApiStr': strCurrWeatherApi, 'locTypeStr': 'current_location'})
	weatherApiArr.append({'weatherApiStr': strBookWeatherApi, 'locTypeStr': 'booking_location'})
	
	#call weather API to retrieve weather
	apiResArr = []
	for apiCall in weatherApiArr:
		r = requests.get(apiCall['weatherApiStr'])
		if r.status_code == 200:
			apiReturnJson = json.loads(r.text)
			
			#Extract fields from API call
			tmpWeatherDict = {}
			tmpWeatherDict.update({'weatherType' : apiReturnJson['currently']['summary']})
			tmpWeatherDict.update({'weatherDescription' : apiReturnJson['currently']['icon']})
			tmpWeatherDict.update({'weatherTemp' : apiReturnJson['currently']['temperature']})
			tmpWeatherDict.update({'windSpeed' : apiReturnJson['currently']['windSpeed']})
			tmpWeatherDict.update({'windGust' : apiReturnJson['currently']['windGust']})
			tmpWeatherDict.update({'windBearing' : apiReturnJson['currently']['windBearing']})
			tmpWeatherDict.update({'rainProb' : apiReturnJson['currently']['precipProbability']})
			apiResArr.append(tmpWeatherDict)
			
			#write to DB
			writeToDB(tmpWeatherDict, apiCall['locTypeStr'],  currLocationArr, eTime)
		else:
			return HttpResponse('Could not call weather API: &s' % r.status_code)
			
	#Write back to screen
	returnStr = {'current_location':{'date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(eTime)), 'type': apiResArr[0]['weatherType'], 'description': apiResArr[0]['weatherDescription'], 'temperature': apiResArr[0]['weatherTemp'], 'wind': {'speed': apiResArr[0]['windSpeed'], 'bearing': apiResArr[0]['windBearing'], 'gust': apiResArr[0]['windGust']}, 'rain_prop': apiResArr[0]['rainProb'], 'latitude': currLocationArr[0], 'logitude': currLocationArr[1]}, 'booking_location': {'date': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(eTime)), 'type': apiResArr[1]['weatherType'], 'description': apiResArr[1]['weatherDescription'], 'temperature': apiResArr[1]['weatherTemp'], 'wind': {'speed': apiResArr[1]['windSpeed'], 'bearing': apiResArr[1]['windBearing'], 'gust': apiResArr[1]['windGust']}, 'rain_prop': apiResArr[0]['rainProb'], 'latitude': bookLocationArr[0], 'logitude': bookLocationArr[1]}}
	return HttpResponse('%s' % (returnStr))

def roundOffDecimal(decimalVal):
	print("Start function - roundOffDecimal")
	returnVal = round(decimalVal,2)
	return returnVal
	print("End function - roundOffDecimal")
	
def writeToDB(dataObj, locationObj, locationArrObj, eTime):
	print("Start function - writeToDB")
	dbData = Location(date = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(eTime)), dayType = dataObj['weatherType'], description = dataObj['weatherDescription'], temperature = roundOffDecimal(Decimal(dataObj['weatherTemp'])), rainProb = int(dataObj['rainProb']), windSpeed = roundOffDecimal(Decimal(dataObj['windSpeed'])), windBearing = roundOffDecimal(Decimal(dataObj['windBearing'])), windGust = roundOffDecimal(Decimal(dataObj['windGust'])), locationType = 'current_location', latitude = Decimal(locationArrObj[0]), longitude =Decimal(locationArrObj[1]))
	dbData.save()
	print("End function - writeToDB")
	