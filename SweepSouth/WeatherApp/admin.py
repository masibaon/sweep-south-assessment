from django.contrib import admin
from .models import LocationType
from .models import Location

# Register your models here.
admin.site.register(LocationType)
admin.site.register(Location)