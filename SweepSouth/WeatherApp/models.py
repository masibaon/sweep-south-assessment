from django.db import models

# Create your models here.
class LocationType(models.Model):
	locationTypeF = models.CharField(max_length=20)
	description = models.CharField(max_length=110)

	#def __str__(self):
		#return self.locationTypeF
	
class Location(models.Model):
	locationType = models.CharField(max_length=20)
	date = models.DateTimeField()
	dayType = models.CharField(max_length=100)
	description = models.CharField(max_length=200)
	temperature = models.DecimalField(max_digits=10, decimal_places=5)
	rainProb = models.IntegerField(default=0)
	latitude = models.DecimalField(max_digits=20, decimal_places=10)
	longitude = models.DecimalField(max_digits=20, decimal_places=10)
	windSpeed = models.DecimalField(max_digits=10, decimal_places=3)
	windBearing = models.DecimalField(max_digits=10, decimal_places=3)
	windGust = models.DecimalField(max_digits=10, decimal_places=3)

	#def __str__(self):
		#return self.locationTypeField